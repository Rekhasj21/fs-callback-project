const fs = require('fs');

function problem2() {
  fs.readFile('lipsum.txt', 'utf8', (err, fileContent) => {
    if (err) throw err;

    const upperContent = fileContent.toUpperCase();
    const upperFileName = 'jsonFiles/upper.txt';

    fs.writeFile(upperFileName, upperContent, (err) => {
      if (err) throw err;

      fs.appendFile('jsonFiles/filenames.txt', `${upperFileName}\n`, (err) => {
        if (err) throw err;

        fs.readFile(upperFileName, 'utf8', (err, response) => {
          if (err) throw err;

          const lowerContent = response.toLowerCase();
          const sentences = lowerContent.split(/[.!?]/);
          const lowerFileName = 'jsonFiles/lower.txt';

          fs.writeFile(lowerFileName, sentences.join('\n'), (err) => {
            if (err) throw err;

            fs.appendFile('jsonFiles/filenames.txt', `${lowerFileName}\n`, (err) => {
              if (err) throw err;

              fs.readFile(lowerFileName, 'utf8', (err, response) => {
                if (err) throw err;

                const sortedContent = response.split('\n').sort().join('\n');
                const sortedFileName = 'jsonFiles/sorted.txt';

                fs.writeFile(sortedFileName, sortedContent, (err) => {
                  if (err) throw err;

                  fs.appendFile('jsonFiles/filenames.txt', `${sortedFileName}\n`, (err) => {
                    if (err) throw err;

                    fs.readFile('jsonFiles/filenames.txt', 'utf8', (err, nameOfFiles) => {
                      if (err) throw err;

                      const fileNames = nameOfFiles.trim().split('\n');
                      let counter = 0;

                      fileNames.forEach((fileName) => {
                        fs.unlink(fileName, (err) => {
                          if (err) throw err;

                          console.log(`Deleted ${fileName}`);
                          counter++;

                          if (counter === fileNames.length) {
                            console.log(`All files deleted.`);
                          }
                        });
                      });
                    });
                  });
                });
              });
            });
          });
        });
      });
    });
  });
}

module.exports =problem2