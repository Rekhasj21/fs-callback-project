const fs = require('fs');
const path = require('path');
const { createRandomJsonFile, deleteDirectory } = require('../problem1')

const dirName = 'jsonFiles';
const numOfFiles = 10


fs.mkdir(dirName, (err) => {
    if (err) {
        console.error(`Error creating directory: ${err}`);
        return;
    }
    console.log(`Created directory ${dirName}`);

    for (let i = 0; i < numOfFiles; i++) {
        createRandomJsonFile(dirName, (err) => {
            if (err) {
                console.error(`Error creating file: ${err}`);
            } else {
                console.log(`Created file ${i + 1}`);
            }
        });
    }
    deleteDirectory(dirName, (err) => {
        if (err) {
            console.error(`Error deleting directory: ${err}`);
            return;
        }
        console.log(`Deleted directory ${dirName}`);
    });
});

