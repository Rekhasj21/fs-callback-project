const fs = require('fs');
const path = require('path');

function createRandomJsonFile(dir, callback) {
    const fileName = `file${Math.ceil(Math.random() * 100)}.json`;
    const filePath = path.join(dir, fileName);
    const content = JSON.stringify({ random: Math.random() * 10 });
    fs.writeFile(filePath, content, callback);

    
    
}

function deleteDirectory(dir, callback) {
    fs.readdir(dir, (err, files) => {
        if (err) {
            callback(err);
            return;
        }
        let count = files.length;
        if (count === 0) {
            fs.rmdir(dir, callback);
            return;
        }
        files.forEach((file) => {
            const filePath = path.join(dir, file);
            fs.unlink(filePath, (err) => {
                if (err) {
                    callback(err);
                } else {
                    count--;
                    if (count === 0) {
                        fs.rmdir(dir, callback);
                    }
                }
            });
        });
    });
}

module.exports = {createRandomJsonFile,deleteDirectory}